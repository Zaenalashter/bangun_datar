<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="src/css/bootstrap.min.css">
    <style>
        body {
            background: url(images/bg_white.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
    <title>Segitiga</title>
</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="card text-center mt-4">
                    <form class="form-signin" action="" method="POST">
                        <img class="mb-4" src="images/logo.png" alt="" width="120" height="120">
                        <h1 class="h3 mb-3 font-weight-normal">Segitiga</h1>
                        <div class="form-row px-1">
                            <input type="number" step="any" id="name" name="tinggi" class="form-control" placeholder="Tinggi" required>
                            <input type="number" step="any" id="name" name="alas" class="form-control" placeholder="Alas" required>
                        </div>
                        <hr>
                        <button class="btn btn-lg btn-primary btn-block" name="luassegitiga" type="submit">Hitung</button>
                        <a href="index.php" class="btn btn-lg btn-danger btn-block" role="button">Kembali</a>
                        <p class="mt-5 mb-3 text-muted">&copy; Zaenalashter | 2021</p>
                    </form>
                </div>
            </div>
            <div class="col-4"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-lg-12">
            <?php
            include "fungsi.php";
            if (isset($_POST['luassegitiga'])) {
                $alas    = $_POST['alas'];
                $tinggi    = $_POST['tinggi'];
                $hasil = Lsegitiga($alas, $tinggi);

                echo "<table class='table'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th scope='col'>Rumus Segitiga</th>";
                echo "<th scope='col'>Tinggi</th>";
                echo "<th scope='col'>Alas</th>";
                echo "<th scope='col'>Luas Segitiga</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                echo "<tr>";
                echo "<td>L = 1/2 x alas x tinggi</td>";
                echo "<td>$alas cm</td>";
                echo "<td>$tinggi cm</td>";
                echo "<td>$hasil cm2</td>";
                echo "</tr>";
                echo "</tbody>";
                echo "</table>";
            }
            ?>
        </div>
    </div>


    <script src="src/js/jquery-3.5.1.slim.min.js"></script>
    <script src="src/js/bootstrap.bundle.min.js"></script>
</body>

</html>