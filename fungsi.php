<?php
//untuk persentase
function persen($segitiga, $persegi, $lingkaran, $for)
{
    $total = $segitiga + $persegi + $lingkaran;
    if ($for == 'segitiga') {
        $hasil = $segitiga / $total * 100;
        return round($hasil, 2);
    } else if ($for == 'persegi') {
        $hasil = $persegi / $total * 100;
        return round($hasil, 2);
    } else {
        $hasil = $lingkaran / $total * 100;
        return round($hasil, 2);
    }
}

//mencari luas segitiga dan menghitung count dan menyimpannya ke file
function Lsegitiga($alas, $tinggi)
{
    $before1 = file_get_contents('countsegitiga.txt');

    $luas = 1 / 2 * $alas * $tinggi;
    $content = array($alas, $tinggi, $luas, date('Y/m/d H:i:s'));
    $count = (int)$before1 + 1;

    file_put_contents("segitiga.txt",  var_export($content, true) . ';', FILE_APPEND | LOCK_EX); //write data to file 
    file_put_contents("countsegitiga.txt",  $count);

    return $luas;
}

//mencari luas persegi dan menghitung count dan menyimpannya ke file
function Lpersegi($sisi)
{
    $before1 = file_get_contents('countpersegi.txt');

    $luas = $sisi * $sisi;
    $content = array($sisi, $luas, date('Y/m/d H:i:s'));
    $count = (int)$before1 + 1;

    file_put_contents("persegi.txt",  var_export($content, true) . ';', FILE_APPEND | LOCK_EX);
    file_put_contents("countpersegi.txt",  $count);

    return $luas;
}

//mencari luas lingkaran dan menghitung count dan menyimpannya ke file
function Llingkaran($jari2)
{
    $before1 = file_get_contents('countlingkaran.txt');

    $luas = 22 / 7 * $jari2 * $jari2;
    $content = array($jari2, $luas, date('Y/m/d H:i:s'));
    $count = (int)$before1 + 1;

    file_put_contents("lingkaran.txt",  var_export($content, true) . ';', FILE_APPEND | LOCK_EX);
    file_put_contents("countlingkaran.txt",  $count);

    return $luas;
}
