<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="src/css/bootstrap.min.css">
    <style>
        body {
            background: url(images/bg_white.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
    <title>Index</title>
</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="card text-center mt-4">
                    <div class="form-signin ">
                        <img class="mb-4 " src="images/logo.png" alt="" width="120" height="120">
                        <h1 class="h3 mb-3 font-weight-normal">Menghitung<br>Bangun Datar</h1>
                        <hr>
                        <a href="segitiga.php" class="btn btn-lg btn-info btn-block btn-danger" role="button">Segitiga</a>
                        <a href="persegi.php" class="btn btn-lg btn-info btn-block" role="button">Persegi</a>
                        <a href="lingkaran.php" class="btn btn-lg btn-info btn-block" role="button">Lingkaran</a>
                        <a href="dashboard.php" class="btn btn-lg btn-warning btn-block" role="button">Dashboard</a>
                        <p class="mt-5 mb-3 text-muted">&copy; Zaenalashter | 2021</p>
                    </div>
                </div>
            </div>
            <div class="col-4"></div>
        </div>
    </div>



    <script src="src/js/jquery-3.5.1.slim.min.js"></script>
    <script src="src/js/bootstrap.bundle.min.js"></script>
</body>

</html>