<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="src/css/bootstrap.min.css">
    <style>
        body {
            background: url(images/bg_white.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
    <title>Dashboard</title>
</head>

<body>
    <?php
    require_once 'fungsi.php';
    $my_arr = file_get_contents('countsegitiga.txt');
    $my_pers = file_get_contents('countpersegi.txt');
    $my_ling = file_get_contents('countlingkaran.txt');
    ?>

    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-10">
                        <h1>Dashboard</h1>
                    </div>
                    <div class="col-2">
                        <a href="index.php" class="btn btn-lg btn-danger btn-block" role="button">Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4 mt-4">
                <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
                    <div class="card-header"><a href="segitiga.php" style="color:white;">Total & Persentase Segitiga</a></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <h2 class="card-title h3"> <?= $my_arr ?></h2>
                            </div>
                            <div class="col-2">
                                <h2>|</h2>
                            </div>
                            <div class="col-6">
                                <h2 class="card-title h3"> <?= persen((int)$my_arr, (int)$my_pers, (int)$my_ling, 'segitiga') ?> %</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-4 mt-4">
                <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
                    <div class="card-header"><a href="persegi.php" style="color:white;">Total & Persentase Persegi</a></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <h2 class="card-title h3"> <?= $my_pers ?></h2>
                            </div>
                            <div class="col-2">
                                <h2>|</h2>
                            </div>
                            <div class="col-6">
                                <h2 class="card-title h3"> <?= persen((int)$my_arr, (int)$my_pers, (int)$my_ling, 'persegi') ?> %</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-4 mt-4">
                <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
                    <div class="card-header"><a href="lingkaran.php" style="color:white;">Total persentase Lingkaran</a></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <h2 class="card-title h3"> <?= $my_ling ?></h2>
                            </div>
                            <div class="col-2">
                                <h2>|</h2>
                            </div>
                            <div class="col-6">
                                <h2 class="card-title h3"> <?= persen((int)$my_arr, (int)$my_pers, (int)$my_ling, 'lingkaran') ?> %</h2>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="src/js/jquery-3.5.1.slim.min.js"></script>
    <script src="src/js/bootstrap.bundle.min.js"></script>
</body>

</html>