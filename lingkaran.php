<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="src/css/bootstrap.min.css">
    <style>
        body {
            background: url(images/bg_white.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
    <title>Lingkaran</title>
</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <div class="card text-center mt-4">
                    <form class="form-signin" action="" method="POST">
                        <img class="mb-4" src="images/logo.png" alt="" width="120" height="120">
                        <h1 class="h3 mb-3 font-weight-normal">Lingkaran</h1>
                        <input type="number" step="any" id="email" name="jari2" class="form-control" placeholder="Jari-Jari" required>
                        <hr>
                        <button class="btn btn-lg btn-primary btn-block" name="luaslingkaran" type="submit">Hitung</button>
                        <a href="index.php" class="btn btn-lg btn-danger btn-block" role="button">Kembali</a>
                        <p class="mt-5 mb-3 text-muted">&copy;Zaenalashter | 2021</p>
                    </form>
                </div>
            </div>
            <div class="col-4"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-lg-12">
            <?php
            include "fungsi.php";
            if (isset($_POST['luaslingkaran'])) {
                $jari2    = $_POST['jari2'];
                $hasil    = Llingkaran($jari2);

                echo "<table class='table'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th scope='col'>Rumus Lingkaran</th>";
                echo "<th scope='col'>Jari - Jari</th>";
                echo "<th scope='col'>Luas Lingkaran</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                echo "<tr>";
                echo "<td>L = 22/7 x Jari2 x Jari2</td>";
                echo "<td>$jari2 cm</td>";
                echo "<td>$hasil cm2</td>";
                echo "</tr>";
                echo "</tbody>";
                echo "</table>";
            }
            ?>
        </div>
    </div>



    <script src="src/js/jquery-3.5.1.slim.min.js"></script>
    <script src="src/js/bootstrap.bundle.min.js"></script>
</body>

</html>